<?php
/**
 * serverw class
 * 
 *  
 * 
 * @author    {author}
 * @copyright {copyright}
 * @package   {package}
 */
class serverwc extends SoapClient {

  private static $classmap = array(
                                   );

  public function serverwc($wsdl = "serviciow.wsdl", $options = array()) {
    foreach(self::$classmap as $key => $value) {
      if(!isset($options['classmap'][$key])) {
        $options['classmap'][$key] = $value;
      }
    }
    parent::__construct($wsdl, $options);
  }
  /**
   * conseguir el PVP del producto 
   *
   * @param string $codigo
   * @return float
   */
  public function getPVP($codigo) {
    return $this->__soapCall('getPVP', array($codigo),       array(
            'uri' => 'http://localhost/~saefesd/01-webServices',
            'soapaction' => ''
           )
      );
  }
  /**
   * obtener el stock de un producto en una tienda concreta 
   *
   * @param string $codigo
   * @param int $tienda
   * @return int
   */
  public function getStock($codigo, $tienda) {
    return $this->__soapCall('getStock', array($codigo, $tienda),       array(
            'uri' => 'http://localhost/~saefesd/01-webServices',
            'soapaction' => ''
           )
      );
  }
  /**
   * obtener las familias de de productos 
   *
   * @param  
   * @return stringArray
   */
  public function getFamilias() {
    return $this->__soapCall('getFamilias', array(),       array(
            'uri' => 'http://localhost/~saefesd/01-webServices',
            'soapaction' => ''
           )
      );
  }
  /**
   * obtener productos de una familia concreta 
   *
   * @param string $familia
   * @return stringArray
   */
  public function getProductosFamilias($familia) {
    return $this->__soapCall('getProductosFamilias', array($familia),       array(
            'uri' => 'http://localhost/~saefesd/01-webServices',
            'soapaction' => ''
           )
      );
  }

}

?>
