<?php
require_once ('DB.php');
require_once ('Producto.php');
class server{
	public function getPVP($codigo){
		$producto = DB::obtieneProducto($codigo);		
		return $producto->getPVP();
	}
	public function getStock($codigo, $tienda){
		$stock = DB::obtieneStock($codigo, $tienda);
		return $stock;
	}
	public function getFamilias(){
		$familias = DB::obtieneFamilias();
		return $familias;
	}
	public function getProductosFamilias($familia){
		$familia = DB::obtieneProductosFamilia($familia);
		return $familia;
	}	
}
?>