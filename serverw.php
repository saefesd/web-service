<?php
require_once ('DB.php');
require_once ('Producto.php');
class serverw{
   /**
   * conseguir el PVP del producto 
   *
   * @param string $codigo
   * @return float
   */
	public function getPVP($codigo){
		$producto = DB::obtieneProducto($codigo);		
		return $producto->getPVP();
	}
  /**
   * obtener el stock de un producto en una tienda concreta 
   *
   * @param string $codigo
   * @param int $tienda
   * @return int
   */
	public function getStock($codigo, $tienda){
		$stock = DB::obtieneStock($codigo, $tienda);
		return $stock;
	}
  /**
   * obtener las familias de de productos 
   *
   * @param  
   * @return string[]
   */
	public function getFamilias(){
		$familias = DB::obtieneFamilias();
		return $familias;
	}
  /**
   * obtener productos de una familia concreta 
   *
   * @param string $familia
   * @return string[]
   */
	public function getProductosFamilias($familia){
		$familia = DB::obtieneProductosFamilia($familia);
		return $familia;
	}	
}
?>